const BASE_URL = "https://63ff4480571200b7b7d9c6f1.mockapi.io";

fetchData("");

function fetchData(key) {
  showLoading();
  axios({
    url: `${BASE_URL}/student`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res.data);
      var studentList = res.data.map(function (student) {
        if (student.name.toUpperCase().indexOf(key.toUpperCase()) > -1) {
          return new Student(
            student.number,
            student.name,
            student.email,
            student.password,
            student.math,
            student.physics,
            student.chemistry
          );
        }
      });
      renderTable(studentList);
      hideLoading();
    })
    .catch(function (err) {
      console.log(err);
      hideLoading();
    });
}

function addStudent() {
  showLoading();
  var student = getFormData();
  axios({
    url: `${BASE_URL}/student`,
    method: "POST",
    data: student,
  })
    .then(function (res) {
      console.log(res.data);
      fetchData("");
    })
    .catch(function (err) {
      console.log(err);
      hideLoading();
    });
}

function removeStudent(number) {
  showLoading();
  axios({
    url: `${BASE_URL}/student/${number}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log(res.data);
      fetchData("");
    })
    .catch(function (err) {
      console.log(err);
      hideLoading();
    });
}

function editStudent(number) {
  showLoading();
  axios({
    url: `${BASE_URL}/student/${number}`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res.data);
      setFormData(res.data);
      hideLoading();
    })
    .catch(function (err) {
      console.log(err);
      hideLoading();
    });
}

function updateStudent() {
  showLoading();
  var student = getFormData();
  axios({
    url: `${BASE_URL}/student/${student.number}`,
    method: "PUT",
    data: student,
  })
    .then(function (res) {
      console.log(res.data);
      fetchData("");
    })
    .catch(function (err) {
      console.log(err);
      hideLoading();
    });
}

function resetForm() {
  document.getElementById("formQLSV").reset();
}

function searchStudent() {
  showLoading();
  var name = document.getElementById("txtSearch").value;
  fetchData(name);
}

document.getElementById("txtSearch").onkeyup = function (e) {
  if (e.key == "Enter") {
    searchStudent();
  }
};
