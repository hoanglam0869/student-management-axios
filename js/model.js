function Student(number, name, email, password, math, physics, chemistry) {
  this.number = number;
  this.name = name;
  this.email = email;
  this.password = password;
  this.math = math;
  this.physics = physics;
  this.chemistry = chemistry;
  this.average = function () {
    return (this.math + this.physics + this.chemistry) / 3;
  };
}
