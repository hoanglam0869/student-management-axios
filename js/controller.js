function getFormData() {
  var number = document.getElementById("txtMaSV").value;
  var name = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var password = document.getElementById("txtPass").value;
  var math = document.getElementById("txtDiemToan").value * 1;
  var physics = document.getElementById("txtDiemLy").value * 1;
  var chemistry = document.getElementById("txtDiemHoa").value * 1;

  return new Student(number, name, email, password, math, physics, chemistry);
}

function renderTable(studentList) {
  document.getElementById("tbodySinhVien").innerHTML = studentList
    .reverse()
    .map(function (student) {
      if (student == undefined) return "";
      return `<tr>
            <td>${student.number}</td>
            <td>${student.name}</td>
            <td>${student.email}</td>
            <td></td>
            <td></td>
            <td>${parseFloat(student.average()).toFixed(2)}</td>
            <td>
                <button onclick="removeStudent('${
                  student.number
                }')" class="btn btn-danger">Xóa</button>
                <button onclick="editStudent('${
                  student.number
                }')" class="btn btn-warning">Sửa</button>
            </td>
        </tr>`;
    })
    .join("");
}

function setFormData(student) {
  document.getElementById("txtMaSV").value = student.number;
  document.getElementById("txtTenSV").value = student.name;
  document.getElementById("txtEmail").value = student.email;
  document.getElementById("txtPass").value = student.password;
  document.getElementById("txtDiemToan").value = student.math;
  document.getElementById("txtDiemLy").value = student.physics;
  document.getElementById("txtDiemHoa").value = student.chemistry;
}

function showLoading() {
  document.getElementById("loading").style.display = "flex";
}

function hideLoading() {
  document.getElementById("loading").style.display = "none";
}
